#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "libreso_system.h"

/*void generateFilename(const char *filename, size_t buffer_length,
				size_t copy_number, size_t copy_total,
				size_t chunks, uint8_t *dest){
	uint8_t hash[MD5_DIGEST_LENGTH];
	MD5(filename, strlen(filename), hash);

	char hex_rep[128];
	snprintf(hex_rep, 128, "%x%x%x%x",
		*((uint32_t*)&hash[0]),
		*((uint32_t*)&hash[4]),
		*((uint32_t*)&hash[8]),
		*((uint32_t*)&hash[12]));

	snprintf(dest, 256, "%s-%d-%d-%d-%d", hex_rep, (int)buffer_length, (int)copy_number, (int)copy_total, (int)chunks);
	printf("generated filename is %s\n", dest);
}*/

void generateFilename(const uint8_t*filename, int32_t copy_number, uint8_t*dest){
	uint8_t *bname, *basec;
	basec = (uint8_t *)strdup((const char *)filename);
	bname = (uint8_t *)basename((char *)basec);
	size_t len_of_filename_gen = strlen((const char *)bname) + 1 + 1 + 1 + 8 + 1; // 1 for -, 1 for copy_num, 1 for -, 8 for 64 bit md5 hash, 1 for /n

	uint8_t hash[MD5_DIGEST_LENGTH];
	MD5((const unsigned char *)filename, strlen((const char *)filename), hash);
	uint8_t hex_rep[64];

	for (uint32_t i = 0; i < 4; i++) {
		sprintf((char * restrict)hex_rep + (2*i),"%02x", hash[3-i]);
	}

	// The above code copies the hex values in the same manner as the below code,
	// but following strict aliasing guidelines for flight code
	// snprintf(hex_rep, 64, "%x%x",
	// 	*((uint32_t*)&hash[0]),
    //             *((uint32_t*)&hash[4]));

	snprintf((char * restrict)dest, len_of_filename_gen, "%s-%d-%s", bname, copy_number, hex_rep);
	printf("generated filename is %s\n", dest);

}

/*void generateFilename(const char *filename, size_t copy_number, uint8_t *dest){
        uint8_t hash[MD5_DIGEST_LENGTH];
        MD5(filename, strlen(filename), hash);

        char hex_rep[128];
        snprintf(hex_rep, 128, "%x%x%x%x",
                *((uint32_t*)&hash[0]),
                *((uint32_t*)&hash[4]),
                *((uint32_t*)&hash[8]),
                *((uint32_t*)&hash[12]));

        snprintf(dest, 256, "%s-%d-%d-%d-%d", hex_rep, copy_number, copy_total, chunks);
        printf("generated filename is %s\n", dest);
}*/

/*int storeFile(char *name, const uint8_t* buffer, size_t size, int copies, int chunks){
	char working_area[256];
	char path[256];
	for(int i = 0; i < copies; i++){
		memset(path, 0, 256);
		memset(working_area, 0, 256);

		snprintf(path, 256, "/var/libreso/folder%d", i);
		snprintf(working_area, 256, "mkdir -p %s", path);
		system(working_area);

		char filename_gen[256];
		generateFilename(name, size, i, copies, chunks, filename_gen);

		char correct_path[256];
		snprintf(correct_path, 256, "%s/%s", path, filename_gen);

		FILE* fd = fopen(correct_path, "w+");
		if(fd == NULL){
			printf("oh shit dawg what you doing: %s", strerror(errno));
			return EXIT_FAILURE;
		}
		for(int c = 0; c < chunks; c++){
			fwrite(buffer, size, 1, fd);
		}
		fclose(fd);
		printf("wrote to file %s\n", correct_path);
	}
	return EXIT_SUCCESS;
}*/

static size_t file_size;
static uint8_t *file_buffer;

int8_t storeFile(uint8_t*name, int32_t copies, int32_t chunks){

	// to prevent undesirable usage of space,
	// #copies and #chunks are restricted to be
	// at most 10
	if(chunks < 1) chunks = 1;
	if(copies < 1) copies = 1;
	if(chunks > 10) chunks = 10;
	if(copies > 10) chunks = 10;

	// read the file to get the buffer to redundantly store
	// use fstat to get length of the above buffer
	uint8_t actual_path[PATH_MAX];
	uint8_t *real_path = (uint8_t*)realpath((const char * restrict)name, (char * restrict)actual_path);
	printf("real_path: %s\n", real_path);

	// check if file can be found
	if(access((const char *)name, R_OK) == -1){
		return FILE_NOT_FOUND;
	}


	// use stat to get size of file
	struct stat st;
	stat((const char * restrict)name, &st);
	size_t size = st.st_size;

	if(file_size < size){
		free(file_buffer);
		file_buffer = NULL;
	}
	if(file_buffer == NULL){
		file_size = size;
		file_buffer = malloc(file_size);
		if(file_buffer == NULL){
			printf("malloc failed");
			return EXIT_FAILURE;
		}
	}

	FILE *input_file_ptr = fopen((const char * restrict)name, "r");
	if(input_file_ptr == NULL){
		printf("whoops can't load the file %s\n", name);
		return EXIT_FAILURE;
	}

	// read file into file_buffer
	int32_t read_bytes = (int32_t)fread(file_buffer, file_size, 1, input_file_ptr);
	if(read_bytes < 0){
		printf("fread() failed with %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

        uint8_t working_area[256];
        uint8_t path[256];

        for(int32_t i = 0; i < copies; i++){
                memset(path, 0, 256);
                memset(working_area, 0, 256);

		// creating directory if doesnt exist
                snprintf((char * restrict)path, 256, "/var/libreso/folder%d", i);
                snprintf((char * restrict)working_area, 256, "mkdir -p %s", path);
                system((const char *)working_area);

                uint8_t filename_gen[256];
                generateFilename(name, i, filename_gen);

                uint8_t correct_path[256];
                snprintf((char * restrict)correct_path, 256, "%s/%s", path, filename_gen);

                FILE* output_file_ptr = fopen((const char * restrict)correct_path, "w+");
                if(output_file_ptr == NULL){
                        printf("oh shit dawg what you doing: %s", strerror(errno));
                        return EXIT_FAILURE;
                }

		// write to created file chunks # of times
                for(int32_t c = 0; c < chunks; c++){
                        fwrite(file_buffer, size, 1, output_file_ptr);
                }

                fclose(output_file_ptr);
		output_file_ptr = NULL;
                printf("wrote to file %s\n", correct_path);
        }
        return EXIT_SUCCESS;
}

static void iterateOverMap(hashMap* map) {
	printf("map->size: %d\n", map->size);
	for(uint32_t i = 0; i < map->size; i++) {
		printf("iteration %d\n", i);
		if(map->elems[i] == NULL) {
			printf("this is null\n");
			continue;
		}
		printf("key %s\n", map->elems[i]->key);
		printf("val %d\n", (*(map->elems[i]->val)));
	}
}

int8_t recallFile(uint8_t*name, int32_t copies, int32_t chunks, uint8_t *buffer, const size_t size){

	// Creating hashmap object
	uint32_t map_size = copies * chunks;
	hashMap* map = malloc(sizeof(hashMap) + (sizeof(hashElem)*map_size));

	int mapBuilt = mapBuild(map, map_size, 1.0,
	                   hashLarson, hashPearson, hashDJB2);

	if(mapBuilt == EXIT_FAILURE) {
		printf("Map was not built\n");
		return EXIT_FAILURE;
	}

	size_t total_elems_in_map = 0;

	uint8_t filename_gen[256];
	uint8_t chunk_buf[size];

	FILE *fp;

	for(int32_t i = 0; i < copies; i++){
			generateFilename(name, i, filename_gen);
			uint8_t filename_tmp[256];
			sprintf((char * restrict)filename_tmp, "/var/libreso/folder%d/%s", i, filename_gen);
			hashElem* tmp_elem = NULL;
			fp = fopen((const char * restrict)filename_tmp, "r");
			if(fp == NULL){
				printf("can't open %s\n", filename_tmp);
			}
			for(int32_t j = 0; j < chunks; j++){
				// read file by chunks
				fread(chunk_buf, size, 1, fp);
				printf("chunk_buf from iteration %d of chunks and %d copy: %s\n", j, i, chunk_buf);



				if(mapFind(map, (uint8_t *)chunk_buf, (uint32_t)size, &tmp_elem) == EXIT_SUCCESS) {
					printf("mapUpdate triggered\n");
					if(tmp_elem == NULL){
						printf("tmp_elem is a NULL\n");
						return EXIT_FAILURE;
					}
					(*(tmp_elem->val))++;
					printf("elem->key in update: %s\n", tmp_elem->key);
					printf("elem->val in update: %d\n", *(tmp_elem->val));
				} else {
					printf("mapInsert triggered\n");
					hashElem *elem = malloc(sizeof(hashElem));
					uint32_t key_len = (uint32_t)size;
					uint32_t val_len = sizeof(int);
					uint8_t* key = malloc(key_len);
					uint8_t* val = malloc(val_len);
					memcpy(key, chunk_buf, key_len);
					int32_t first_freq = 1;
					memcpy(val, &first_freq, sizeof(int));
					elem->key = key;
					elem->val = val;
					printf("elem->key: %s\n", elem->key);
					printf("elem->val: %d\n", *(elem->val));
					elem->key_len = key_len;
					elem->val_len = val_len;
					mapInsert(map, elem, NULL);
					total_elems_in_map++;
				}
			}
			fclose(fp);
	}

	printf("hash map stuff done\n");
	iterateOverMap(map);
	// hash map stuff done
	// do voting algorithm
	// write to buffer user passed
	// or write NULL and return not found or whatever
	// or return a struct with a confidence rating on top 3 vals

	// write function that sorts the list of frequencies
	// and then returns the buffer that has the highest frequency

	printf("total_elems_in_map: %d\n", (int)total_elems_in_map);
	hashElem *sortedList = malloc(total_elems_in_map * sizeof(hashElem));
	uint8_t* max_key = findMaxKey(map, total_elems_in_map, sortedList);
	printf("max key: %s\n", max_key);
	memcpy(buffer, max_key, size);

	return EXIT_SUCCESS;

}

int8_t deleteFile(uint8_t *name, size_t buf_len, int32_t copies, int32_t chunks){
    (void)buf_len; (void)chunks;
	uint8_t filename_gen[256];
	int32_t files_del = 0;
	for(int32_t i = 0; i < copies; i++){
		generateFilename(name, i, filename_gen);
		uint8_t filename_tmp[256];
		sprintf((char * restrict)filename_tmp, "/var/libreso/folder%d/%s", i, filename_gen);
		if(remove((const char *)filename_tmp) == 0){
			//printf("File %s deleted successfully\n", filename_gen);
 			files_del++;
		} else {
			//printf("Unable to delete file %s\n", filename_gen);
		}
	}
	return (files_del == copies) ? EXIT_SUCCESS : EXIT_FAILURE;
}

// comparator for qsort
int cmp(const void *e1, const void *e2){
	const hashElem* temp1 = (const hashElem*) e1;
	const hashElem* temp2 = (const hashElem*) e2;
	int32_t a1 = *(temp1->val);
	int32_t a2 = *(temp2->val);
	if(a1 < a2) return -1;
	if(a1 == a2) return 0;
	else return 1;
}


void sortMapByFreq(hashMap *map, size_t total_elems_in_map, hashElem* sortedList){
	uint32_t k = 0;
	for(uint32_t i = 0; i < map->size; i++) {
		if(map->elems[i] != NULL) {
			sortedList[k] = *(map->elems[i]);
			k++;
		}
	}
	for(uint32_t i = 0; i < total_elems_in_map; i++) {
		printf("sortedList elem %d\n", i);
		printf("key: %s\n", sortedList[i].key);
		printf("val: %d\n", *(sortedList[i].val));
	}
	qsort(sortedList, total_elems_in_map, sizeof(hashElem), cmp);
}

uint8_t* findMaxKey(hashMap* map, size_t total_elems_in_map, hashElem *sortedList){
        sortMapByFreq(map, total_elems_in_map, sortedList);
	size_t max_key_len = sortedList[total_elems_in_map-1].key_len;
	uint8_t* buf = malloc(max_key_len + 1);
        memcpy(buf, sortedList[total_elems_in_map-1].key, max_key_len);
	return buf;
}


/*

int main() {
	// name, buffer, size, copies, chunks

	// use realpath() to pass in the absolute path for testing
	// actual user should do something else
	char *relative_path_tanya = "../tanya.txt";
	char *relative_path_you = "you.txt";
//	char actual_path_tanya[PATH_MAX], actual_path_you[PATH_MAX];
//	char *ptr_tanya;
//	char *ptr_you;
//	ptr_tanya = realpath(relative_path_tanya, actual_path_tanya);
//	ptr_you = realpath(relative_path_you, actual_path_you);
//	printf("ptr_tanya: %s\n", ptr_tanya);
	//storeFile(relative_path_tanya, 4, 2);
//	storeFile(ptr_you, 4, 2);

	// testing recall
  	// name copies chunks buffer size
	struct stat st;
	char *ptr_tanya = relative_path_tanya;
        stat(ptr_tanya, &st);
        size_t size = st.st_size;

	char* file_buffer_main = malloc(size);
	char* file_buffer_to_pass = malloc(size);

        FILE *input_file_ptr = fopen(ptr_tanya, "r");
        if(input_file_ptr == NULL){
                printf("whoops can't load the file inside main%s\n", ptr_tanya);
                return EXIT_FAILURE;
        }

        int read_bytes = fread(file_buffer_main, size, 1, input_file_ptr);
        if(read_bytes < 0){
                printf("fread() failed with %s\n inside main", strerror(errno));
                return EXIT_FAILURE;
        }

	recallFile(ptr_tanya, 4, 2, file_buffer_to_pass, size);
	printf("recovered file content buffer: %s\n", file_buffer_to_pass);
//	printf("actual file content buffer: %s\n", file_buffer_main);
	//

	int ret = deleteFile(relative_path_tanya, size, 4, 2);
	if (ret == EXIT_SUCCESS)
		printf("all deleted\n");

	return 0;
}*/
