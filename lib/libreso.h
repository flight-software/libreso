#ifndef _LIB_RESO_H_
#define _LIB_RESO_H_

//Formerly in libhashmap.h
#include <stdint.h>         /* uintX_t */
#include <string.h>         /* memcmp, memset */

#define FEC_SYNC        (UINT32_C(0xa00bcdfe))

#define RS_SYM_LEN  (8    ) /* Symbol length in bits */
#define RS_DATA_LEN (223  ) /* Data length in RS packet, in bytes */
#define RS_MSG_LEN  (255  ) /* Total length of RS packet, including parity */
#define RS_DET_ERRS (16   ) /* Number of detectable symbol errors */
#define RS_GEN_POLY (0x11d) /* Generator Polynomial Coefficients (read up on Galois fields if you're interested) */
#define RS_FCS      (1    ) /* Various RS settings for the Galois field we use. These are only used in initRS, which is external code */
#define RS_PRIM     (1    )
#define RS_NROOTS   (32   )
#define EXIT_MAP_FULL 2
#define EXIT_MAP_CONFLICT 3


//=================================================================
//Fomerly in libhashmap.h

typedef struct {
    uint8_t * key;
    uint8_t * val;
    uint32_t key_len;
    uint32_t val_len;
    uint32_t prb_len;

    uint32_t cache1;
    uint32_t cache2;
    uint32_t cache3;
} hashElem;

typedef struct {
    uint32_t size;
    uint32_t maxProbeLength;
    uint32_t (*hashFn1)(uint8_t*, uint32_t);
    uint32_t (*hashFn2)(uint8_t*, uint32_t);
    uint32_t (*hashFn3)(uint8_t*, uint32_t);
    hashElem * elems[];

} hashMap;

/**
 * A private helper struct to help with
 * update and delete operations
 */
typedef struct {
  hashElem* elem;
  uint32_t idx;
} Helper;

/* mapBuild: constructs a hash map given a size and a multiplier,
 * and 3 hash functions
 * map_size: the "given" size of the map
 * size_multiplier: Factor to multiply the size by for the real number of elements
 * hashFn1,2,3: The hash functions used to characterize the key
 * hashFn1 is used for index calculations, and hashFn2,3 are used for equality checking
 *
 * Return value - indicates success
 *
 *
 * Edge cases accounted for:
 * given map pointer is NULL
 * given map_size is NULL
 * given map_size is negative
 * Any of the given hash functions are NULL
*/
int8_t mapBuild(hashMap * map, uint32_t map_size, float size_multiplier,
                   uint32_t (*hashFn1)(uint8_t*, uint32_t),
                   uint32_t (*hashFn2)(uint8_t*, uint32_t),
                   uint32_t (*hashFn3)(uint8_t*, uint32_t));

/* mapInsert: inserts an element into a map given their pointers
* map: pointer to the map we insert into
* elem: pointer to the element we insert into
* ret: a pointer to a hashElem* if there already exists one with an element in the map with same key as elem
*
* Return value - indicates success
*
*
* Edge cases accounted for:
* given elem's key is NULL
* given elem's key_len is 0
* given element already exists in the map (by key)
* Also tests if ret itself is NULL
*/
int8_t mapInsert(hashMap * map, hashElem * elem, hashElem** ret);

/* mapUpdate: given a map and an element, find the element with the same key
              and update its data to match that of the given element
 * map: pointer to the map we are updating
 * elem: pointer to the element whose data is being "added" to the map
 * ret: a pointer to a hashElem* whose value is updated
 *
 * Return value - indicates success
 *
 *
 * Edge cases accounted for:
 * given elem pointer is NULL
 * given elem's key is NULL
 * given elem's key length is 0
*/
int8_t mapUpdate(hashMap * map, hashElem * elem, hashElem** ret);

/* mapFind: given a map and a key with its length, find the element in map
            with the key
 * map: pointer to the map we are updating
 * key: key whose matching element we are finding
 * len: length of key
 * ret: a pointer to the found hash element
 *
 * Return value - indicates success
 *
 *
 * Edge cases accounted for:
 * given map pointer is NULL
 * given key pointer is NULL
 * given key length is 0
 * element doesn't exist - returns NULL
*/
int8_t mapFind(hashMap * map, uint8_t * key, uint32_t len, hashElem** ret);

/* mapDeleteWithElem: given a map and an element, find the element
 * in the map and delete it
 *
 * map: pointer to the map we are updating
 * elem: pointer to the element being deleted
 * ret: a pointer to the hash element removed from the table
 *
 * Return value - indicates success
 *
 *
 * Edge cases accounted for:
 * given elem pointer is NULL
 * given map pointer is NULL
 * given elem's key length is 0
 * given elem's key is NULL
 * given element does not exist in the map (checked in helperUpdateDel)
*/
int8_t mapDeleteWithElem(hashMap * map, hashElem * elem, hashElem** ret);

/* mapDeleteWithKey: given a map and a key with its length, find the element
 * in the map with the matching key and delete it
 *
 * map: pointer to the map we are updating
 * key: key of the element we are trying to delete\
 * key_len: length of key
 * ret: a pointer to the hash element removed from the table
 *
 * Return value - indicates success
 *
 *
 * Edge cases accounted for:
 * given map pointer is NULL
 * given key length is 0
 * given key is NULL
 * given element does not exist in the map (checked in helperUpdateDel)
*/
int8_t mapDeleteWithKey(hashMap * map, uint8_t * key, uint32_t key_len, hashElem** ret);

/* hashLarson: given a buffer, perform the Larson hashing algorithm on it
 *
 * buf: buffer to be hashed
 * buf_len: length of buf
 *
 * Return value - hash value
 *
 *
 * Edge cases accounted for:
 * given buffer is NULL
*/
extern uint32_t hashLarson(uint8_t * buf,uint32_t buf_len);

/* hashPearson: given a buffer, perform the Pearson hashing algorithm on it
 *
 * buf: buffer to be hashed
 * buf_len: length of buf
 *
 * Return value - hash value
 *
 *
 * Edge cases accounted for:
 * given buffer is NULL
*/
extern uint32_t hashPearson(uint8_t * buf, uint32_t buf_len);

/* hashDJB2: given a buffer, perform the DJB2 hashing algorithm on it
 *
 * buf: buffer to be hashed
 * buf_len: length of buf
 *
 * Return value - hash value
 *
 *
 * Edge cases accounted for:
 * given buffer is NULL
*/
extern uint32_t hashDJB2(uint8_t * buf,uint32_t buf_len);

/* helperUpdateDel: given a map and a key with its length,
 * returns a struct with a pointer to the element with the key, and how far
 * into the hash table the function had to look to find it. If not found,
 * return a NULL pointer and 0 distance
 *
 * map: pointer to the map
 * key: key whose element we are looking for
 * key_len: length of key in bytes
 *
 * Return value - returns a struct with a pointer to the element with the key, and how far
 * into the hash table the function had to look to find it
 *
 *
 * Edge cases accounted for:
 * given key doesn't match any element in the map
*/
void helperUpdateDel(hashMap * map, uint8_t * key, uint32_t len, Helper* h);

/*
 * heSwap: given 2 elem pointers, swap them
*/
void heSwap(hashElem * e1, hashElem * e2);

/* buildHashElem: given an element pointer and a key and value, build the
 * element and return its pointer back

 *
 * elem: pointer to the elem being built
 * key: key of the element
 * key_len: length of key in bytes
 * val: value of the element
 * val_len: length of val in bytes
 *
 * Return value - pointer to the built element
 *
 *
 * Edge cases accounted for:
 * elem pointer is NULL
*/
hashElem* buildHashElem(hashElem* elem, uint8_t* key, uint32_t key_len, uint8_t* val, uint32_t val_len);

//Formerly in libhashmap.h
//======================================================================

typedef struct {
    struct rs * rs;
    uint32_t sync;
    //fec q;
    int nerrs;
} FEC_OBJECT;

extern FEC_OBJECT rs_fec;

int rsInit(FEC_OBJECT *f);
int rsDestroy(FEC_OBJECT *f);
int rsEncode(FEC_OBJECT * f, uint8_t * src, uint8_t * dest);
int rsDecode(FEC_OBJECT * f, uint8_t * src, uint8_t * dest);

extern int storeRedundant(const char ** file_name, const uint8_t * buffer,
                            uint8_t num_files, uint8_t repetitions,
                            uint8_t buffer_size, uint8_t no_cache);
extern int recallRedundant(const char ** file_name, const uint8_t * buffer,
                            uint8_t num_files, uint8_t repetitions,
                            uint8_t buffer_size);

#define SAT_ID_FILE_ARR {"/etc/sat_id/iter_0", "/etc/sat_id/iter_1", \
                            "/etc/sat_id/iter_2", "/etc/sat_id/iter_3", \
                            "/etc/sat_id/iter_4"}
#define HOST_ID_FILE_ARR {"/etc/host_id/iter_0", "/etc/host_id/iter_1", \
                            "/etc/host_id/iter_2"}
#define SESSION_ID_FILE_ARR {"/etc/session_id/iter_0"}
#define SESSION_NUM_FILE_ARR {"/etc/sat_num/iter_0"}

#define IN_PARAM_LIST \
    PARAM(SatelliteID, 32, SAT_ID_FILE_ARR, 5, 3, 1) \
    PARAM(HostID, 16, HOST_ID_FILE_ARR, 3, 3, 0) \
    PARAM(SessionID, 16, SESSION_ID_FILE_ARR, 1, 1, 0) \
    PARAM(SessionNum, 16, SESSION_NUM_FILE_ARR, 1, 1, 0)

// Attempts to store the relevant parameter to val
// Returns 0 on success
// Returns 1 and sets *val to 0 on failure
#define PARAM(NAME, VAL, FILE_ARR, FILE_ARR_SIZE, IN_FILE_REP, NO_CACHE) \
extern int refec_get##NAME(uint##VAL##_t * val); \
extern int refec_set##NAME(uint##VAL##_t val);
IN_PARAM_LIST
#undef PARAM

#endif /* _LIB_RESO_H_ */
