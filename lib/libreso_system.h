#ifndef LIBRESO_SYSTEM_H
#define LIBRESO_SYSTEM_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <libgen.h>
#include <sys/types.h>
#include <openssl/ssl.h>
#include <openssl/md5.h>
#include "libreso.h"

#define SIZE_MULTIPLIER 1.0
#define FILE_NOT_FOUND 4


/*
	libreso_system is intended to be run as root
*/


//########################################################


/*
	SUMMARY:
	Stores a file in multiple places, with each
	of those copies containing multiple repetitions
	of the data.

	DIRECTORY STRUCTURE:
	/var/libreso contains multiple folders.
	/var/libreso/folder0 contains the first copy
	of all files stored.
	/var/libreso/folder1 contains the 2nd copy
	of all files stored.
	and so on.
	If more copies need to be stored than the number
	of folders currently available, a new folder
	will be created automatically.

	PARAMETER USAGE:

	1. uint8_t*name
		There are two ways in which this parameter
		can be used.

		Option 1: This option is especially useful
		if the file to be stored is not present in		  the directory in which the libreso_system 		    program is being run, and cannot be
		directly accessed by the program.
		In this case, the RELATIVE PATH or the
		ABSOLUTE PATH can be specified.

		Option 2: If the user does not choose to
		specify the relative or absolute path,
		they can specify only the name of the
		file. In this case, it is the user's
		responsibility to ensure that the file
		can be accessed by the program.

		If the file cannot be found or accessed,
		the program exits with FILE_NOT_FOUND.

	2. int8_t copies
		The number of copies of the file that
		are created.

	3. int chunks
		The number of times the file data is
		repeated in every instance of a copy of
		the file.

	FILENAMES:
	The generateFilename function is used to generate
	filenames.
	The format of a filename is:
	[BASENAME]-[COPY]-[FIRST_HALF_OF_MD5_HASH_OF_NAME]

	The reasons for choosing this filename are
	detailed in the generateFilename function docs.

	RETURN VALUES:

	1. EXIT_SUCCESS: The file was successfully stored
	copies # of times, with each copy containing
	the data chunks # of times.

	2. FILE_NOT_FOUND: The file to store could not
	be found. Either this file does not exist, or
	it cannot be accessed in the scope of this program	  perhaps because the relative path or the absolute
	path is not specified.

	3. EXIT_FAILURE: For all other cases of failure.

	OTHER NOTES:
	To prevent undesirable space use, the number of
	copies and the number of chunks are restricted
	to be at most 10.

*/
int8_t storeFile(uint8_t*name, int32_t copies, int32_t chunks);

/*
	Uses the voting algorithm to return the
	most probable data that the file contained.

	PARMETERS:
	uint8_t* buffer: This is written to by the fn.
	The value of the most probable data is written
	here. This must be allocated by the function
	that calls recallFile()

	const size_t size: This is the size of the buf.
*/
int8_t recallFile(uint8_t*name, int32_t copies, int32_t chunks, uint8_t *buffer, const size_t size);

/*
	Deletes all the copies of the given file.

	PARAMETERS:
	uint8_t*name: relative path, absolute path, or name
	size_t buf_len: length of the file to delete
	copies and chunks have same meaning.

	RETURN VALUES:
	EXIT_SUCCESS: If all files (copies # of files)
	are successfully deleted.
	EXIT_FAILURE: If not all files are deleted.
*/
int8_t deleteFile(uint8_t*name, size_t buf_len, int32_t copies, int32_t chunks);

/* --------------------------Helper Functions -------------------------- */

/*
	Format:
	[BASENAME]-[COPY]-[FIRST_HALF_OF_MD5_HASH_OF_NAME]

	[BASENAME]:
	If the relative or absolute path of the file is
	specified in the filename, only the name of the
	file (ie the string after the last /) is used.

	Else if only the filename is specified, it is
	directly used.

	[COPY]:
	The copy number of this file.

	[FIRST_HALF_OF_MD5_HASH_OF_NAME]:
	First half (64 bits) of MD5 hash of the filename.
	This filename could actually be a relative path,
	absolute path, or just the name of the file.

	MOTIVATION FOR PICKING THIS FORMAT:
	1. Human readability and searching by name:
	Storing the basename and the copy number allows
	humans to easily understand what file is being
	stored, as well as search for a file by it's name.

	2. Collision resolution:
	For users that want to ensure that collisions
	don't occur, they should pass in the relative
	or the absolute path to the file they want to
	store. The md5 hash of this path that is appended
	to the basename-copy string will ensure that
	the entire filename is unique.
	Only 64 bits (out of 128) are used, because
	it is unlikely that there will be many collisions
	so 64 bits should be enough to uniquely identify
	a file.

*/
void generateFilename(const uint8_t*filename, int32_t copy_number, uint8_t*dest);

uint8_t* findMaxKey(hashMap* map, size_t total_elems_in_map, hashElem* sortedList);

/*
	Sorts map by value (ie frequency of occurence of a chunk) and stores
	the sorted list of elements in sortedList.

	map: the hashMap to sort, this map contains chunks and the frequency with
	which the chunk occurs. map->size = copies * chunks, where copies is the
	number of files in which the given file is stored, and chunks is the number
	of times in each file the same chunk is repeated.
	the true number of elements that the map contains is always less than or
	equal to the map->size.
	the true number of elements is the number of unique chunks.
	in an ideal world with zero bit flips, the true number of elements will be 1
	because every chunk in every file will be the same.

	total_elems_in_map: the true number of elements

	sortedList: user needs to allocate this list. this list is allocated in
	recallFile. the size of this list is (total_elems_in_map * sizeof(hashElem).
	this list will contain all the non null elems of map.
	this list will then be passed into qsort and qsort will actually sort the
	list in place.

	tl;dr to get a sorted list from a map, pass in the map pointer, the true
	number of elems in the map, and an empty array of hashElems to obtain the
	elems sorted by value
*/
void sortMapByFreq(hashMap* map, size_t total_elems_in_map, hashElem* sortedList);

// comparator for qsort
int cmp(const void *e1, const void *e2);
#endif
