#include <stdio.h>          /* fprintf, fopen, FILE, remove */
#ifdef DEBUG
#include <string.h>         /* strerror */
#endif
#include <stdlib.h>
#include <syslog.h>
#include <errno.h>
#include "libreso.h"
#include "fec.h"
FEC_OBJECT rs_fec;

//==============================================================================

int rsDestroy(FEC_OBJECT *f)
{
    if(f == NULL) return EXIT_FAILURE;

  //  fec_destroy(f->q);

    if(f->sync == FEC_SYNC)
        free_rs_char(f->rs);

    f->sync = ~FEC_SYNC;
    return EXIT_SUCCESS;
}


//============================================================================

int rsInit(FEC_OBJECT *f)
{
    if(f == NULL) return EXIT_FAILURE;

    //puts("");
    if(f->sync == FEC_SYNC) {
        #ifdef DEBUG
        //puts("Attempting to re-initialize rsObject. Skipping.");
        #endif
        return EXIT_SUCCESS; // we've already created the object
    }
    f->sync = FEC_SYNC;

  //  fec_scheme fs = LIQUID_FEC_RS_M8;   // error-correcting scheme
  //  f->q = fec_create(fs, NULL);

    f->rs = init_rs_char(RS_SYM_LEN
            , RS_GEN_POLY
            , RS_FCS
            , RS_PRIM
            , RS_NROOTS
            , 0 // Never pad, we keep that metadata ourselves
            );
    f->nerrs = 0;

    return EXIT_SUCCESS;
}

//==============================================================================

int rsEncode(FEC_OBJECT *f, uint8_t *src, uint8_t *dest)
{
    if(f == NULL) return EXIT_FAILURE;
    if(f->sync != FEC_SYNC) {
        #ifdef DEBUG
        syslog(LOG_ERR, "FEC object uninitialize rsObject.");
        #endif
        return EXIT_FAILURE;
    }
    // Encode the src buffer into the dest buffer
    //fec_encode(f->q, RS_DATA_LEN, src, dest);
    memcpy(dest, src, RS_DATA_LEN);
    encode_rs_char(f->rs, dest, &dest[RS_DATA_LEN]);
    return EXIT_SUCCESS;
}


//==============================================================================

int rsDecode(FEC_OBJECT *f, uint8_t * src, uint8_t * dest)
{
    if(f == NULL) return EXIT_FAILURE;
    if(f->sync != FEC_SYNC) {
        #ifdef DEBUG
        puts("FEC object uninitialize rsObject.");
        #endif
        return EXIT_FAILURE;
    }
    uint8_t inter_buf[RS_MSG_LEN];
    // Decode the src buffer into the dest buffer
#if 0
    fec_decode(f->q, RS_MSG_LEN, src, dest);
    void * zeros = calloc(RS_MSG_LEN, sizeof(unsigned char));
    //if(memcmp(zeros, (*f)->derrlocs))
    free(zeros);
#else
    memcpy(inter_buf, src, RS_MSG_LEN);
    int derrlocs[RS_MSG_LEN];
    int nerrs = decode_rs_char(f->rs, inter_buf, derrlocs, 0);
    f->nerrs = nerrs;
    if(nerrs > RS_DET_ERRS) return EXIT_FAILURE;
    memcpy(dest, inter_buf, RS_DATA_LEN);
#endif
    return EXIT_SUCCESS;
}

//==============================================================================

int storeRedundant(const char ** file_name, const uint8_t * buffer,
                    uint8_t num_files, uint8_t repetitions,
                    uint8_t buffer_size, uint8_t no_cache)
{
    int exit_mode = EXIT_SUCCESS;
    if(RS_DATA_LEN < buffer_size) {
        #ifdef DEBUG
        fprintf(stderr, "Invalid buffer size: buffer above max length.\n");
        #endif
        goto exit;
    }
    uint8_t h_buf[RS_DATA_LEN];
    uint8_t w_buf[RS_MSG_LEN];
    FILE * fd_arr[1 << 8];
    uint8_t mk_fid = 0;
    uint8_t w_fid = 0;
    uint8_t delfiles = 0;
    // Create files
    for(; mk_fid < num_files; mk_fid++) {
        if(NULL == (*(fd_arr + mk_fid) = fopen(*(file_name + mk_fid), "wb+"))) {
            #ifdef DEBUG
            fprintf(stderr, "Error creating file: %s\n", strerror(errno));
            #endif
            exit_mode = EXIT_FAILURE;
            goto revert;
        }
    }
    for(;w_fid < num_files; w_fid++) {
        if(no_cache || w_fid == 0) {
            for(uint8_t iter = 0; iter < RS_DATA_LEN/buffer_size; iter++) {
                memcpy(h_buf + buffer_size * iter, buffer, buffer_size);
            }
            if(rsEncode(&rs_fec, h_buf, w_buf)) {
                #ifdef DEBUG
                fprintf(stderr, "Error encoding buffer.\n");
                #endif
                exit_mode = EXIT_FAILURE;
                goto revert;
            }
        }
        for(uint8_t re_val = 0; re_val < repetitions; re_val++) {
            fwrite(w_buf, sizeof(uint8_t), RS_MSG_LEN, *(fd_arr + w_fid));
            if(ferror(*(fd_arr + w_fid))) {
                #ifdef DEBUG
                fprintf(stderr, "Error writing to file: %s\n", strerror(errno));
                #endif
                exit_mode = EXIT_FAILURE;
                goto revert;
            }
        }
    }
    goto cleanup;
revert:
    delfiles = 1;
cleanup:
    for(; mk_fid-- > 0;) {
        fclose(*(fd_arr + mk_fid));
        #ifdef DEBUG
        fprintf(stderr, "Error closing file: %s\n", strerror(errno));
        #endif
        if(delfiles && -1 == remove(*(file_name + mk_fid))) {
            #ifdef DEBUG
            fprintf(stderr, "Error deleting file: %s\n", strerror(errno));
            #endif
        }
    }
exit:
    return exit_mode;
}

int recallRedundant(const char ** file_name, const uint8_t * buffer,
                    uint8_t num_files, uint8_t repetitions,
                    uint8_t buffer_size)
{
    uint8_t max_n_types = (uint8_t)RS_DATA_LEN/buffer_size;
    (void)max_n_types;
    (void)file_name;
    (void)buffer;
    (void)num_files;
    (void)repetitions;
    (void)buffer_size;
    return EXIT_SUCCESS;
}

//=============================================================================
//Formerly in libhashmap.c


int8_t mapBuild(hashMap * map, uint32_t map_size, float size_multiplier,
                   uint32_t (*hashFn1)(uint8_t*, uint32_t),
                   uint32_t (*hashFn2)(uint8_t*, uint32_t),
                   uint32_t (*hashFn3)(uint8_t*, uint32_t))
{
    if (map_size == 0) return EXIT_FAILURE;
    if (map == NULL) return EXIT_FAILURE;
    if (hashFn1 == NULL) return EXIT_FAILURE;
    if (hashFn2 == NULL) return EXIT_FAILURE;
    if (hashFn3 == NULL) return EXIT_FAILURE;

    if (size_multiplier < 1.0f) size_multiplier = 1.0f;
    if (size_multiplier > 1.5f) size_multiplier = 1.5f;

    map_size = (uint32_t) ((float)map_size * size_multiplier);
    if (map_size > 10000) return EXIT_FAILURE;
    // Based on Kenny's back-of-an-envelope math.
    // We've generally found counts around 20k segfault

    map->hashFn1 = hashFn1;
    map->hashFn2 = hashFn2;
    map->hashFn3 = hashFn3;
    map->size = map_size;
    for(uint32_t i = 0; i < map_size; i++) {
        map->elems[i] = NULL;
    }
    map->maxProbeLength = 0;

    return EXIT_SUCCESS;
}


int8_t mapInsert(hashMap * map, hashElem * elem, hashElem** ret)
{
  if (elem == NULL) return EXIT_FAILURE;
  if (map == NULL) return EXIT_FAILURE;
  if (elem->key == NULL) return EXIT_FAILURE;
  if (elem->key_len == 0) return EXIT_FAILURE;

  elem->cache1 = map->hashFn1(elem->key, elem->key_len);
  elem->cache2 = map->hashFn2(elem->key, elem->key_len);
  elem->cache3 = map->hashFn3(elem->key, elem->key_len);






  elem->prb_len = 0;
  uint32_t distance = 0;
  uint32_t desiredPos = elem->cache1 % map->size;

  while (1) {
    if (map->elems[desiredPos] == NULL) {
      map->elems[desiredPos] = elem;
      if (elem->prb_len > map->maxProbeLength) map->maxProbeLength = elem->prb_len;
      return EXIT_SUCCESS;
    }

    //if control reaches here, there is a collision OR the element already exists
    //NOTE: Do equality checking here

    if (elem->cache1 == map->elems[desiredPos]->cache1) {
      if (elem->cache2 == map->elems[desiredPos]->cache2) {
        if (elem->cache3 == map->elems[desiredPos]->cache3) {
          if (ret != NULL) *ret = map->elems[desiredPos];
          return EXIT_MAP_CONFLICT;
        }
      }
    }


    if ((map->elems[desiredPos])->prb_len < elem->prb_len) {
      //printf("Swapping elements...\n");
      heSwap(elem, map->elems[desiredPos]);
    }

    desiredPos++;
    elem->prb_len++;
    desiredPos = desiredPos % map->size;

    distance++;
    if (distance > map->size) break;
  }

  return EXIT_MAP_FULL;
}


int8_t mapUpdate(hashMap * map, hashElem * elem, hashElem** ret)
{
  if(map == NULL || elem == NULL || elem->key == NULL || elem->key_len == 0) {
    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
  }

  Helper updateHelper;
  helperUpdateDel(map, elem->key, elem->key_len, &updateHelper);

  if (updateHelper.elem == NULL) {
    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
  }

  elem->cache1 = (updateHelper.elem)->cache1;
  elem->cache2 = (updateHelper.elem)->cache2;
  elem->cache3 = (updateHelper.elem)->cache3;
  elem->prb_len = (updateHelper.elem)->prb_len;
  map->elems[updateHelper.idx] = elem;

  if (ret != NULL) *ret = updateHelper.elem;
  return EXIT_SUCCESS;

}


int8_t mapFind(hashMap * map, uint8_t * key, uint32_t len, hashElem** ret)
{
  if(map == NULL || key == NULL || len == 0) {
    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
  }

  uint32_t desiredPos = map->hashFn1(key, len) % map->size;
  uint32_t distance = 0;
  uint32_t currentPos = desiredPos;

  while (distance < map->size) {
    if (map->elems[currentPos] != NULL) {
      if (map->hashFn2(key, len) == (map->elems[currentPos])->cache2) {
        if (map->hashFn3(key, len) == (map->elems[currentPos])->cache3) {
            if (ret != NULL) *ret = map->elems[currentPos];
            return EXIT_SUCCESS;
        }
      }
    }
    distance++;
    currentPos = (desiredPos + distance) % map->size;
  }

    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
}


int8_t mapDeleteWithElem(hashMap * map, hashElem * elem, hashElem** ret)
{
  if(map == NULL || elem == NULL || elem->key == NULL || elem->key_len == 0) {
    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
  }


  Helper deleteHelper;
  helperUpdateDel(map, elem->key, elem->key_len, &deleteHelper);
  if(deleteHelper.elem != NULL) {
    map->elems[deleteHelper.idx] = NULL;
    if (ret != NULL) *ret = deleteHelper.elem;
    return EXIT_SUCCESS;
  } else {
    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
  }
}


int8_t mapDeleteWithKey(hashMap * map, uint8_t * key, uint32_t key_len, hashElem** ret)
{
  if(map == NULL || key == NULL || key_len == 0) {
    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
  }

  Helper deleteHelper;
  helperUpdateDel(map, key, key_len, &deleteHelper);

  if(deleteHelper.elem != NULL) {
    map->elems[deleteHelper.idx] = NULL;
    if (ret != NULL) *ret = deleteHelper.elem;
    return EXIT_SUCCESS;
  } else {
    if (ret != NULL) *ret = NULL;
    return EXIT_FAILURE;
  }
}


uint32_t hashLarson(uint8_t * buf,uint32_t buf_len)
{
  uint32_t hash = 1;
  uint8_t curr;
  for (size_t i = 0; i < buf_len; i++) {
    if ((buf + i) != NULL) {
      curr = *(buf + i);

    } else return hash;
    // This is any case the buffer reaches NULL for any reason
    hash = hash * 101 + curr;
  }
  return hash;
}


uint32_t hashPearson(uint8_t * buf, uint32_t buf_len)
{
    uint8_t hashPart;
    uint32_t hash = 0;
    static const uint8_t pearsonHashTable[256] = {       // 0-255 shuffled in any (random) order suffices
        98,  6, 85,150, 36, 23,112,164,135,207,169,  5, 26, 64,165,219, //  1
        61, 20, 68, 89,130, 63, 52,102, 24,229,132,245, 80,216,195,115, //  2
        90,168,156,203,177,120,  2,190,188,  7,100,185,174,243,162, 10, //  3
       237, 18,253,225,  8,208,172,244,255,126,101, 79,145,235,228,121, //  4
       123,251, 67,250,161,  0,107, 97,241,111,181, 82,249, 33, 69, 55, //  5
        59,153, 29,  9,213,167, 84, 93, 30, 46, 94, 75,151,114, 73,222, //  6
       197, 96,210, 45, 16,227,248,202, 51,152,252,125, 81,206,215,186, //  7
        39,158,178,187,131,136,  1, 49, 50, 17,141, 91, 47,129, 60, 99, //  8
       154, 35, 86,171,105, 34, 38,200,147, 58, 77,118,173,246, 76,254, //  9
       133,232,196,144,198,124, 53,  4,108, 74,223,234,134,230,157,139, // 10
       189,205,199,128,176, 19,211,236,127,192,231, 70,233, 88,146, 44, // 11
       183,201, 22, 83, 13,214,116,109,159, 32, 95,226,140,220, 57, 12, // 12
       221, 31,209,182,143, 92,149,184,148, 62,113, 65, 37, 27,106,166, // 13
         3, 14,204, 72, 21, 41, 56, 66, 28,193, 40,217, 25, 54,179,117, // 14
       238, 87,240,155,180,170,242,212,191,163, 78,218,137,194,175,110, // 15
        43,119,224, 71,122,142, 42,160,104, 48,247,103, 15, 11,138,239  // 16
       };

       // The original algorithm outputted a 32-bit hash so the iterator
       // counted  i < 8, so I changed to i < 4. I'm not sure if any other
       // modifications are necessary.
       if (buf == NULL) return hash;
       for(size_t i = 0; i < 4; i++) {
          hashPart = pearsonHashTable[(buf[0]+i) % 256];
          for (size_t j = 1; j < buf_len; j++) {
            if (buf+j ==NULL) return hash;
            hashPart = pearsonHashTable[hashPart ^ buf[j]];
          }
          hash = hash | hashPart << (24 - (8*i));
       }

    return hash;
}


uint32_t hashDJB2(uint8_t * buf, uint32_t buf_len)
{
  uint32_t hash = 5381;

  for (size_t i = 0; i < buf_len; i++) { // Assumes buf_len counts bytes, not elements
    if (buf + i == NULL) return hash;
    hash = ((hash << 5) + hash) + buf[i];
  }
  return hash;
}



void helperUpdateDel(hashMap * map, uint8_t * key, uint32_t len, Helper* h)
{
  uint32_t desiredPos = map->hashFn1(key, len) % map->size;
  uint32_t hash2 = map->hashFn2(key, len);
  uint32_t hash3 = map->hashFn3(key, len);
  uint32_t distance = 0;
  uint32_t currentPos = desiredPos;

  while (distance < map->size) {
    if (map->elems[currentPos] != NULL) {
      if (hash2 == (map->elems[currentPos])->cache2) {
        if (hash3 == (map->elems[currentPos])->cache3) {
            *h = (Helper){map->elems[currentPos], currentPos};
            return;
        }
      }
    }
    distance++;
    currentPos = (desiredPos + distance) % map->size;
  }
  *h = (Helper){NULL, 0};
}


void heSwap(hashElem * e1, hashElem * e2)
{
    if(e1 == e2) {
        return;
    }
    hashElem h;
    memcpy(&h, e1, sizeof(hashElem));
    memcpy(e1, e2, sizeof(hashElem));
    memcpy(e2, &h, sizeof(hashElem));

    return;
}


hashElem* buildHashElem(hashElem* elem, uint8_t* key, uint32_t key_len, uint8_t* val, uint32_t val_len) {
  if (elem == NULL) return NULL;
  elem-> key = key;
  elem-> key_len = key_len;
  elem->val = val;
  elem-> val_len = val_len;
  elem->cache3 = 0;
  elem->cache2 = 0;
  elem->cache1 = 0;
  elem->prb_len = 0;
  return elem;
}

//Formerly in libhashmap.c
//============================================================================

/*

#define PARAM(NAME, SIZE, FILE_ARR, FILE_ARR_SIZE, IN_FILE_REP, NO_CACHE) \
int refec_get##NAME(uint##SIZE##_t * val) \
{ \
    return recallRedundant((const char * []) FILE_ARR, (uint8_t *) val, \
                            FILE_ARR_SIZE, IN_FILE_REP, \
                            sizeof(uint##SIZE##_t)/sizeof(uint8_t)); \
} \
int refec_set##NAME(uint##SIZE##_t val) \
{ \
    return storeRedundant((const char * []) FILE_ARR, (uint8_t *) &val, \
                            FILE_ARR_SIZE, IN_FILE_REP, \
                            sizeof(uint##SIZE##_t)/sizeof(uint8_t), \
                            NO_CACHE); \
}
IN_PARAM_LIST
#undef PARAM

*/
